(function() {
    'use strict';

    angular.module('app.config', [])
        .constant('BASEURL',
            'http://localhost:8080/Portafolio-Backend/webresources')
        .config(configure);

    configure.$inject = ['$authProvider', 'BASEURL'];

    function configure($authProvider, BASEURL) {
        // Parametros de configuración Satellizer
        $authProvider.loginUrl = BASEURL + '/auth/login';
        $authProvider.tokenName = 'token';
        $authProvider.tokenPrefix = 'Portafolio';
    }
})();
