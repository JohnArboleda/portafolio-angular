(function() {
    'use strict';
    angular
        .module('app.servicios.services', [])
        .factory('Servicios', Servicios);

    Servicios.$inject = ['$resource', 'BASEURL'];


    //Este servicio nos provee de los métodos GET POST PUT DELETE
    function Servicios($resource, BASEURL) {
        return $resource(BASEURL + '/servicios/:id', {
            id: '@id'
        });
    }

    //De igual manera los factory nos sirven para almacenar información
    //y que nos pueda servir en cualquier controlador o lugar de la aplicación
    //evitando de esta manera hacer variables globales.
})();
