(function () {
    'use strict';

    angular.module('app.servicios.directivas', [

    ]).directive('serviciosList', serviciosList)
    .directive('serviciosCreate', serviciosCreate);

    function serviciosList() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/servicios/list.html',
            controller: 'ServiciosListCtrl',
            controllerAs: 'vm'
        };
    }

    function serviciosCreate(){
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/servicios/create.html',
            controller: 'ServiciosCreateCtrl',
            controllerAs: 'vm'
        };
    }
})();
