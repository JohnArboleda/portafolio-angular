(function() {
    'use strict';

    angular.module('app.usuarios.controller', []).controller('usuariosCreateCtrl', UsuariosCreateCtrl)
        .controller('usuariosListCtrl', UsuariosListCtrl)
        .controller('UsuariosUpdateCtrl', UsuariosUpdateCtrl)
    .controller('UsuariosViewCtrl', UsuariosViewCtrl);

    UsuariosCreateCtrl.$inject = ['$location', '$mdToast', 'Usuarios', 'Ciudades', 'Roles'];

    function UsuariosCreateCtrl($location, $mdToast, Usuarios, Ciudades, Roles) {
        var vm = this;

        vm.create = create;
        vm.queryCiudades = queryCiudades;
        vm.loadFoto = loadFoto;
        vm.roles = Roles.query();
        vm.dateMax = new Date();
        vm.dateMax.setFullYear(vm.dateMax.getFullYear() - 18);

        function create() {
            Usuarios.save(vm.usuario, function() {
                $location.path('/usuarios');
                $mdToast.show(
                    $mdToast.simple()
                    .textContent('Se ha  guardado el Usuario...')
                    .position('top right'));
            }, function(error) {
                $mdToast.show(
                    $mdToast.simple()
                    .textContent(error.status + ' ' + error.data)
                    .position('top right'));
            });
        }

        function queryCiudades(str) {
            return Ciudades.queryByNombre({
                query: str
            });
        }

        function loadFoto($fileContent) {
            vm.usuario.foto = $fileContent;
        }
    }

    UsuariosListCtrl.$inject = ['$location', 'Usuarios'];

    function UsuariosListCtrl($location, Usuarios) {
        var vm = this;
        vm.query = {
            limit: 5,
            page: 1
        };
        vm.usuarios = Usuarios.query();

    }

    UsuariosUpdateCtrl.$inject = ['$stateParams', '$location', '$mdToast', 'Usuarios'];

    function UsuariosUpdateCtrl($stateParams, $location, $mdToast, Usuarios) {
        var vm = this;
        vm.id = $stateParams.idUsuario;
        vm.update = update;
        vm.usuario = Usuarios.get({
            idUsuario: vm.id
        });

        function update() {
            Usuarios.update(vm.usuario).$promise
                .then(function() {
                    $location.path('/usuarios');
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Se ha  actualizado el  Usuario...')
                        .position('bottom right'));
                });
        }
    }

    UsuariosUpdateCtrl.$inject = ['$stateParams', '$q', 'Usuarios'];

    function UsuariosViewCtrl($stateParams, $q, Usuarios) {
        var vm = this;
        vm.id = $stateParams.idUsuario;

        activate();

        function activate() {
            var promises = [getUsuario()];
            return $q.all(promises).then(function() {
            });
        }

        function getUsuario() {
          return Usuarios.get({
              idUsuario: vm.id
          }).$promise.then(function(data){
            vm.usuario = data;
            vm.usuario.fechaNac = new Date(vm.usuario.fechaNac);
            return vm.usuario;
          });
        }
    }

})();
