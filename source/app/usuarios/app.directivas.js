(function () {
    'use strict';

    angular.module('app.usuarios.directivas', [])
    .directive('usuarioslist', usuariosList)
    .directive('usuarioscreate', usuariosCreate)
    .directive('usuariosupdate', usuariosUpdate)
    .directive('usuariosview', usuariosView);

    function usuariosList() {
        return {
            scope: {},
            templateUrl: 'app/usuarios/list.html',
            restrict: 'EA',
            controller: 'usuariosListCtrl',
            controllerAs: 'vm'
        };
    }

    function usuariosCreate(){
        return {
            scope: {},
            templateUrl: 'app/usuarios/create.html',
            restrict: 'EA',
            controller: 'usuariosCreateCtrl',
            controllerAs: 'vm'
        };
    }

    function usuariosUpdate(){
        return {
            scope: {},
            templateUrl: 'app/usuarios/update.html',
            restrict: 'EA',
            controller: 'UsuariosUpdateCtrl',
            controllerAs: 'vm'
        };
    }
    function usuariosView(){
        return {
            scope: {},
            templateUrl: 'app/usuarios/view.html',
            restrict: 'EA',
            controller: 'UsuariosViewCtrl',
            controllerAs: 'vm'
        };
    }
})();
