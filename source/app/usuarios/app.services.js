(function() {
    'use strict';
    angular.module('app.usuarios.services', [])
        .factory('Usuarios', Usuarios)
        .factory('Ciudades', Ciudades)
        .factory('Roles', Roles);

    Usuarios.$inject = ['$resource', 'BASEURL'];

    function Usuarios($resource, BASEURL) {
        return $resource(BASEURL + '/usuarios/:idUsuario', {
            idUsuario: '@idUsuario'
        }, {
            'update': {
                method: 'PUT'
            }
        });
    }

    Ciudades.$inject = ['$resource', 'BASEURL'];

    function Ciudades($resource, BASEURL) {
        return $resource(BASEURL + '/ciudades/:idCiudad', {
            idCiudad: '@idCiudad'
        },{
          queryByNombre: {
            url: BASEURL + '/ciudades/nombre/:query',
            method: 'GET',
            isArray: true,
            params: {
              query: '@query'
            }
          }
        });
    }

    Roles.$inject = ['$resource', 'BASEURL'];

    function Roles($resource, BASEURL) {
      return $resource(BASEURL + '/roles');
    }

})();
