(function() {
    'use strict';

    angular.module('app', [
        'ui.router',
        'ngResource',
        'angular.filter',
        'ngMaterial',
        'ngMessages',
        'ngTable',
        'md.data.table',
        'satellizer',
        //'templates',
        'app.config',
        'app.imagenes',
        'app.inicio',
        'app.footer',
        'app.header',
        'app.usuarios',
        'app.categorias',
        'app.login',
        'app.servicios'
    ]);

})();
