(function () {
    'use strict';

    angular.module('app.inicio.controller', [
    ])
    .controller('inicioCtrl', InicioCtrl);


    function InicioCtrl() {
        this.lenguajes = [
            {nombre: 'Java'},
            {nombre: 'Javascript'},
            {nombre: 'Python'}
        ];

    }
})();
