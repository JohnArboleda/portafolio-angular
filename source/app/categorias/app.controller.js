(function() {
    'use strict';

    angular.module('app.categorias.controller', []).controller('categoriasCreateCtrl', CategoriasCreateCtrl)
        .controller('categoriasListCtrl', CategoriasListCtrl)
        .controller('categoriasUpdateCtrl', CategoriasUpdateCtrl);

    CategoriasCreateCtrl.$inject = ['$location', '$mdToast', 'Categorias'];

    function CategoriasCreateCtrl($location, $mdToast, Categorias) {
        this.create = function() {
            Categorias.save(this.categoria, function() {
                $location.path('/categorias');
                $mdToast.show(
                    $mdToast.simple()
                    .textContent('Se ha  guardado la categoria...')
                    .position('bottom right'));
            });
        };
    }

    CategoriasListCtrl.$inject = ['$location', 'Categorias'];

    function CategoriasListCtrl($location, Categorias) {
        this.query = {
            order: 'name',
            limit: 5,
            page: 1
        };
        this.categorias = Categorias.query();
    }

    CategoriasUpdateCtrl.$inject = ['$stateParams', '$location', '$mdToast', 'Categorias'];

    function CategoriasUpdateCtrl($stateParams, $location, $mdToast, Categorias) {
        this.id = $stateParams.idCategoria;
        this.categoria = Categorias.get({
            idCategoria: this.id
        });


        this.update = function() {
            Categorias.update(this.categoria, function() {
                $location.path('/categorias');
                $mdToast.show(
                    $mdToast.simple()
                    .textContent('Se ha  actualizado la categoria...')
                    .position('bottom right'));
            });
        };
    }

})();
